
var bankBalance = 48;
var workButton = document.getElementById('work');
var loanButton = document.getElementById('loan');
var buyButton = document.getElementById('buy-btn');

var balance = document.getElementById('balance')

workButton.addEventListener('click', function () {
    bankBalance += 10;
    balance.innerHTML = 'Current balance: ' + bankBalance + "kr";
});

loanButton.addEventListener('click', function () {
    var neededMoney = document.getElementById('computer-list').value
    bankBalance = neededMoney;
    loanButton.disabled = true;
    loanButton.innerHTML = 'Parents are now broke. :('
    balance.innerHTML = 'Current balance: ' + bankBalance + "kr";
});

buyButton.addEventListener('click', function () {
    var neededMoney = document.getElementById('computer-list').value
    if (bankBalance >= neededMoney) {
        alert('Congratulations! You are now the proud owner of a new computer!');
        bankBalance = bankBalance - neededMoney;
        balance.innerHTML = 'Current balance: ' + bankBalance + "kr";
    } else {
        alert("Sorry dude, you're not welcome here anymore!");
        buyButton.disabled = true;
        buyButton.innerHTML = 'Access denied!';
    };
});